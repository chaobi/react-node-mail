# react-node-mail

A Basic ReactJs & NodeJs app for sending emails via Mailgun and Sendgrid

### Prerequsites

* Install `node` ( v8.* )
* Install `yarn` ( v1.7.0 )

### To Install

* `yarn` - install dependencies

### To Run Developer Environment

* `yarn start-dev` - This will do Transpiling, Building and running the server. It will watch for the changes and repeat the process.

### In Production

* `yarn start` -  This will do Transpiling, Building and running the server. Well you dont have to worry about this. Most of the servers will take care of this.
