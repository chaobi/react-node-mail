import React, {Component} from 'react';
import { FormGroup, ControlLabel, FormControl, HelpBlock, Button} from 'react-bootstrap';


function FieldGroup({ id, label, help, ...props }) {
    return (
      <FormGroup controlId={id}>
        <ControlLabel>{label}</ControlLabel>
        <FormControl {...props} />
        {help && <HelpBlock>{help}</HelpBlock>}
      </FormGroup>
    );
}

export default class EmailForm extends React.Component {
    constructor(props, context) {
      super(props, context);
  
      this.state = {
        value: ''
      };
    }
  
    render() {
      return (
        <form action="/api/email" method="POST">
            <FieldGroup
                id="formControlsEmailTo"
                name="to"
                type="email"
                label="To"
                placeholder="Enter emails"
                multiple
                required
            />
            <FieldGroup
                id="formControlsEmailCc"
                name="cc"
                type="email"
                label="Ccs"
                placeholder="Enter emails"
                multiple
            />
            <FieldGroup
                id="formControlsEmailBcc"
                name="bcc"
                type="email"
                label="Bccs"
                placeholder="Enter emails"
                multiple
            />
             <FieldGroup
                id="formControlsEmail"
                name = "subject"
                type="text"
                label="Subject"
                placeholder="Enter subject"
                required
            />
            <FormGroup controlId="formControlsTextarea">
                <ControlLabel>Body</ControlLabel>
                <FormControl name="text" componentClass="textarea" placeholder="textarea" required/>
            </FormGroup>

            <Button type="submit">Submit</Button>
        </form>
      );
    }
  }