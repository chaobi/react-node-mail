//Dependencies
import path from 'path';
import express from 'express';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';

require('dotenv').config();
var emailApi = require('../../controller/EmailApi');  //Todo : copy controllers to build folder
var app = express();

const publicPath = express.static(path.join(__dirname, '../'));
const indexPath = path.join(__dirname, '../index.html');
const port = process.env.PORT || 5000;

//Configure Body Pareser and Cookie Parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.use(publicPath);

app.get('/', (req, res) => {
    res.sendFile(indexPath);
})

app.use('/api/email', emailApi);

app.listen(port, function(){
  console.log(`Listening at http://localhost:${port}`);
});
