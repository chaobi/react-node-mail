var express = require('express')
var router = express.Router()
var bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({extended: true}));
router.use(bodyParser.json());

function SendViaMailgun(data){
  var mailgun = require("mailgun-js");
  var api_key = process.env.MG_API_KEY;
  var DOMAIN = process.env.DOMAIN_NAME;
  var mailgun = require('mailgun-js')({apiKey: api_key, domain: DOMAIN});
 
  data.from = 'Excited User <me@mg.botanyaccountants.com>';
  console.log(data);
  mailgun.messages().send(data, function (error, body) {
    console.log(body);
    console.log(error);
  });
}

  // define the home page route
  router.post('/', function (req, res) {
  
  SendViaMailgun(req.body);

  res.send('success');
})
// define the about route
router.get('/Ping', function (req, res) {
  res.send('/Pong')
})

module.exports = router